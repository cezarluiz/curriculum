Cezar Luiz
==========

My personal CV in JSON format.

Contact: cezarluiz.c@gmail.com


```json
{
  "name": "Cezar Luiz Sampaio",
  "age": "21 years old",
  "email": "cezarluiz.c@gmail.com",
  "year career": "4 years",
  "networks": {
    "twitter": "@cezar_luiz",
    "github": "CezarLuiz0",
    "linked_in": "http://www.linkedin.com/pub/cezar-luiz/43/907/453"
  },
  "skills": {
    "front-end": [
      "HTML5",
      "CSS3",
      "Javascript",
      "Ajax",
      "jQuery",
      "AngularJS",
      "Responsive Web Design",
      "Cross-browser compliance"
    ],
    "back-end": [
      "OOP PHP",
      "Wordpress",
      "Slim Framework",
      "Laravel",
      "OAuth"
    ],
    "sql": [
      "MySQL",
      "MongoDB"
    ],
    "patterns": [
      "RESTFul",
      "Javascript Module Pattern",
      "MVC",
      "Web Standards",
      "Progressive enhancement"
    ],
    "apis": [
      "Facebook",
      "Twitter",
      "Any other (JSON or XML)"
    ],
    "version-control": [
      "Git",
      "SVN"
    ],
    "UX/UI": [
      "Flows",
      "Wireframing",
      "Information Architecture",
      "Paper/Rapid Prototyping",
      "Usability Testing",
      "Card Sorting",
      "Axure",
      "Mind Node"
    ]
  },
  "education": [
    {
      "Positivo University": [
        {
          "course": "Analysis and Systems Development",
          "duration": "3 years",
          "finshed at": "end of 2013"
        }
      ]
    }
  ],
  "employments": [
    {
      "Agência IMAM": {
        "position": "Production Manager",
        "time period": "2011 - Present",
        "site": "www.agenciaimam.com.br",
        "tags": [
          "PHP",
          "HTML5",
          "CSS3",
          "Wordpress",
          "Slim Framework",
          "Laravel",
          "Mobile Apps",
          "Subversion",
          "Javascript",
          "RESTful APIs"
        ],
        "description": "I'm the responsible for the manage a small development team, planning, development, testing, launch and maintenance of the projects made in the agency. I use the most recents web technologies, like HTML5, CSS3 transitions, pure Javascript, AJAX applications and micro PHP frameworks. "
      }
    }
  ],
  "colaborations": [
    {
      "project": "Dive into HTML5",
      "site": "diveintohtml5.com.br",
      "colaboration": "Translation of chapter: 'Você Está Aqui (Assim Como Todo Mundo Está)'"
    },
    {
      "project": "Brower Diet",
      "site": "browserdiet.com",
      "colaboration": "Inserted a sprite tool in the documentation (Stitches)"
    }
  ]
}
```
